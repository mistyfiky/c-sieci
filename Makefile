CC=gcc
CFLAGS=-Wall

main: main.c
	$(CC) $(CFLAGS) $^ -o $@

all: main

distclean:
	rm -f main
