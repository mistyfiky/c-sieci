#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <math.h>

#define LETTER_HEIGHT 7
#define LETTER_WIDTH 5
#define LETTER_SIZE LETTER_HEIGHT * LETTER_WIDTH
#define INPUTS LETTER_SIZE
#define OUTPUTS 12
#define SIM_BETA 5
#define TRAIN_REPEATS 1000

typedef struct {
    int size;
    double *x;
} VECTOR;

typedef struct {
    int rows;
    int cols;
    double *x;
} MATRIX;

// HELPERS

void randomize ();

int get_random_int (int min, int max);

double get_random_double (double min, double max);

// VECTOR

VECTOR *vector_new (int size);

double vector_get (VECTOR v, int index);

double vector_set (VECTOR *v, int index, double value);

void vector_print (VECTOR v, char *label);

MATRIX *vector_to_matrix (VECTOR v);

VECTOR *vector_unitary (int size, double value);

VECTOR *vector_substract (VECTOR a, VECTOR b);

VECTOR *vector_element_wise_multiplication (VECTOR a, VECTOR b);

// MATRIX

MATRIX *matrix_new (int rows, int cols);

int matrix_idx (MATRIX m, int row, int col);

double matrix_get (MATRIX m, int row, int col);

double matrix_set (MATRIX *m, int row, int col, double value);

void matrix_print (MATRIX m, char *label);

VECTOR *matrix_to_vector (MATRIX m, int col);

MATRIX *matrix_complex_conjugate_transpose (MATRIX m);

MATRIX *matrix_scalar_product (MATRIX a, MATRIX b);

MATRIX *matrix_unitary (int rows, int cols, double value);

MATRIX *matrix_identity (int size);

MATRIX *matrix_clone (MATRIX m);

MATRIX *matrix_addition (MATRIX a, MATRIX b);

// MAIN

MATRIX *init (int inputs, int outputs);

VECTOR *sim (MATRIX weights, VECTOR input, double beta);

MATRIX *train (MATRIX weights, VECTOR example, VECTOR t, double beta, double factor);

int main (int argc, char **argv);
