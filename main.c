#include "main.h"

double letter_A_x[] = {
    0, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 1, 1, 1, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
};
VECTOR letter_A = {
    .size=LETTER_SIZE,
    .x=(double *) &letter_A_x
};

double letter_B_x[] = {
    1, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 1, 1, 1, 0,
};
VECTOR letter_B = {
    .size=LETTER_SIZE,
    .x=(double *) &letter_B_x
};

double letter_C_x[] = {
    0, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 1,
    0, 1, 1, 1, 0,
};
VECTOR letter_C = {
    .size=LETTER_SIZE,
    .x=(double *) &letter_C_x
};

double letter_pyt_x[] = {
    0, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    0, 0, 0, 0, 1,
    0, 0, 1, 1, 0,
    0, 0, 1, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 1, 0, 0,
};
VECTOR letter_pyt = {
    .size=LETTER_SIZE,
    .x=(double *) &letter_pyt_x
};

double letter_E_x[] = {
    1, 1, 1, 1, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 1, 1, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 1, 1, 1, 0,
};
VECTOR letter_E = {
    .size=LETTER_SIZE,
    .x=(double *) &letter_E_x
};

double letter_F_x[] = {
    1, 1, 1, 1, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 1, 1, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
    1, 0, 0, 0, 0,
};
VECTOR letter_F = {
    .size=LETTER_SIZE,
    .x=(double *) &letter_F_x
};

double letter_G_x[] = {
    0, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 0,
    1, 0, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    0, 1, 1, 1, 0,
};
VECTOR letter_G = {
    .size=LETTER_SIZE,
    .x=(double *) &letter_G_x
};

double letter_H_x[] = {
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 1, 1, 1, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
};
VECTOR letter_H = {
    .size=LETTER_SIZE,
    .x=(double *) &letter_H_x
};

double letter_S_x[] = {
    0, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 0,
    0, 1, 1, 1, 0,
    0, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    0, 1, 1, 1, 0,
};
VECTOR letter_S = {
    .size=LETTER_SIZE,
    .x=(double *) &letter_S_x
};

double letter_T_x[] = {
    1, 1, 1, 1, 1,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
};
VECTOR letter_T = {
    .size=LETTER_SIZE,
    .x=(double *) &letter_T_x
};

double letter_Y_x[] = {
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    0, 1, 0, 1, 0,
    0, 1, 0, 1, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 1, 0, 0,
};
VECTOR letter_Y = {
    .size=LETTER_SIZE,
    .x=(double *) &letter_Y_x
};

double letter_X_x[] = {
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    0, 1, 0, 1, 0,
    0, 0, 1, 0, 0,
    0, 1, 0, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
};
VECTOR letter_X = {
    .size=LETTER_SIZE,
    .x=(double *) &letter_X_x
};

double letter_challenge_x[] = {
    1, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 1,
    1, 1, 1, 1, 0,
    1, 0, 0, 0, 1,
    1, 0, 0, 0, 0.45,
    1, 1, 1, 1, 0,
};
VECTOR letter_chanllenge = {
    .size=LETTER_SIZE,
    .x=(double *) &letter_challenge_x
};


// HELPER

void randomize ()
{
  time_t t;
  srand ((unsigned) time (&t));
}

int get_random_int (int min, int max)
{
  return (rand () % (max - min)) + min;
}

double get_random_double (double min, double max)
{
  return min + (rand () / (RAND_MAX / (max - min)));
}

// VECTOR

VECTOR *vector_new (int size)
{
  VECTOR *r = (VECTOR *) malloc (sizeof (VECTOR));
  r->size = size;
  r->x = (double *) malloc (size * sizeof (double));
  return r;
}

double vector_get (VECTOR v, int index)
{
  return v.x[index];
}

double vector_set (VECTOR *v, int index, double value)
{
  double r = vector_get (*v, index);
  v->x[index] = value;
  return r; // old value
}

void vector_print (VECTOR v, char *label)
{
  if (label != NULL) {
    printf ("%s\n", label);
  }
  for (int index = 0; index < v.size; index++) {
    printf ("\t%f", vector_get (v, index));
  }
  printf ("\n");
}

MATRIX *vector_to_matrix (VECTOR v)
{
  MATRIX *r = matrix_new (v.size, 1);
  for (int index = 0; index < v.size; index++) {
    matrix_set (r, index, 0, vector_get (v, index));
  }
  return r;
}

VECTOR *vector_unitary (int size, double value)
{
  VECTOR *r = vector_new (size);
  for (int index = 0; index < size; index++) {
    vector_set (r, index, value);
  }
  return r;
}

VECTOR *vector_substract (VECTOR a, VECTOR b)
{
  VECTOR *r = vector_new (a.size);
  for (int index = 0; index < a.size; index++) {
    vector_set (r, index, vector_get (a, index) - vector_get (b, index));
  }
  return r;
}

VECTOR *vector_element_wise_multiplication (VECTOR a, VECTOR b)
{
  VECTOR *r = vector_new (a.size);
  for (int index = 0; index < a.size; index++) {
    vector_set (r, index, vector_get (a, index) * vector_get (b, index));
  }
  return r;
}

// MATRIX

MATRIX *matrix_new (int rows, int cols)
{
  MATRIX *r = (MATRIX *) malloc (sizeof (MATRIX));
  r->rows = rows;
  r->cols = cols;
  r->x = (double *) malloc (rows * cols * sizeof (double));

  return r;
}

int matrix_idx (MATRIX m, int row, int col)
{
  return row * m.cols + col;
}

double matrix_get (MATRIX m, int row, int col)
{
  return m.x[matrix_idx (m, row, col)];
}

double matrix_set (MATRIX *m, int row, int col, double value)
{
  double r = matrix_get (*m, row, col);
  m->x[matrix_idx (*m, row, col)] = value;
  return r; // old value
}

void matrix_print (MATRIX m, char *label)
{
  if (label != NULL) {
    printf ("%s\n", label);
  }
  for (int row = 0; row < m.rows; row++) {
    for (int col = 0; col < m.cols; col++) {
      printf ("\t%f", matrix_get (m, row, col));
    }
    printf ("\n");
  }
}

VECTOR *matrix_to_vector (MATRIX m, int col)
{
  VECTOR *r = vector_new (m.rows);
  for (int row = 0; row < m.rows; row++) {
    vector_set (r, row, matrix_get (m, row, col));
  }
  return r;
}

MATRIX *matrix_complex_conjugate_transpose (MATRIX m)
{
  MATRIX *r = matrix_new (m.cols, m.rows);
  for (int row = 0; row < m.rows; row++) {
    for (int col = 0; col < m.cols; col++) {
      matrix_set (r, col, row, -matrix_get (m, row, col));
    }
  }
  return r;
}

MATRIX *matrix_scalar_product (MATRIX a, MATRIX b)
{
  double s;
  MATRIX *r = matrix_new (a.rows, b.cols);
  for (int row = 0; row < a.rows; row++) {
    for (int col = 0; col < b.cols; col++) {
      s = 0;
      for (int i = 0; i < a.cols; i++) {
        s += matrix_get (a, row, i) * matrix_get (b, col, i);
      }
      matrix_set (r, row, col, s);
    }
  }
  return r;
}

MATRIX *matrix_unitary (int rows, int cols, double value)
{
  MATRIX *r = matrix_new (rows, cols);
  for (int row = 0; row < rows; row++) {
    for (int col = 0; col < cols; col++) {
      matrix_set (r, row, col, value);
    }
  }
  return r;
}

MATRIX *matrix_identity (int size)
{
  MATRIX *r = matrix_new (size, size);
  for (int row = 0; row < size; row++) {
    for (int col = 0; col < size; col++) {
      matrix_set (r, row, col, row == col ? 1 : 0);
    }
  }
  return r;
}

MATRIX *matrix_clone (MATRIX m)
{
  MATRIX *r = matrix_new (m.rows, m.cols);
  for (int row = 0; row < m.rows; row++) {
    for (int col = 0; col < m.cols; col++) {
      matrix_set (r, row, col, matrix_get (m, row, col));
    }
  }
  return r;
}

MATRIX *matrix_addition (MATRIX a, MATRIX b)
{
  MATRIX *r = matrix_new (a.rows, a.cols);
  for (int row = 0; row < a.rows; row++) {
    for (int col = 0; col < a.cols; col++) {
      matrix_set (r, row, col, matrix_get (a, row, col) + matrix_get (b, row, col));
    }
  }
  return r;
}

// MAIN

MATRIX *init (int inputs, int outputs)
{
  MATRIX *r = matrix_new (inputs, outputs);
  for (int row = 0; row < inputs; row++) {
    for (int col = 0; col < outputs; col++) {
      matrix_set (r, row, col, get_random_double (-0.1, 0.1));
    }
  }
  return r; // init weights
}

VECTOR *sim (MATRIX weights, VECTOR input, double beta)
{
  double fu;
  VECTOR *r, *u_v;
  MATRIX *weights_t, *input_m, *u;
  weights_t = matrix_complex_conjugate_transpose (weights);
  input_m = vector_to_matrix (input);
  u = matrix_scalar_product (*weights_t, *input_m);
  u_v = matrix_to_vector (*u, 0);
  r = vector_new (u_v->size);
  for (int index = 0; index < u_v->size; index++) {
    fu = 1 / (1 + exp (-beta * vector_get (*u_v, index)));
    vector_set (r, index, fu);
  }
  return r; // output
}

MATRIX *train (MATRIX weights, VECTOR example, VECTOR t, double beta, double factor)
{
  VECTOR *y, *y_i, *d, *e;
  MATRIX *r, *dw, *e_m, *e_m_t, *factor_m, *example_m;
  example_m = vector_to_matrix (example);
  factor_m = vector_to_matrix (*vector_unitary (example.size, factor));
  y = sim (weights, example, SIM_BETA);
  d = vector_substract (t, *y);
  e = vector_element_wise_multiplication (*d, *vector_unitary (d->size, beta));
  e = vector_element_wise_multiplication (*e, *y);
  y_i = vector_substract (*vector_unitary (d->size, 1), *y);
  e = vector_element_wise_multiplication (*e, *y_i);
  dw = matrix_scalar_product (*factor_m, *example_m);
  e_m = vector_to_matrix (*e);
  e_m_t = matrix_complex_conjugate_transpose (*e_m);
  dw = matrix_scalar_product (*dw, *e_m_t);
  r = matrix_addition (weights, *dw);
  return r; // new weights
}

int main (int argc, char **argv)
{
  int index;
  MATRIX *weights, *identity_matrix = matrix_identity (OUTPUTS);
  VECTOR *result, *t, examples[] = {
      letter_A,
      letter_B,
      letter_C,
      letter_pyt,
      letter_E,
      letter_F,
      letter_G,
      letter_H,
      letter_S,
      letter_T,
      letter_Y,
      letter_X,
  };
  randomize ();

  weights = init (INPUTS, OUTPUTS);
  result = sim (*weights, letter_chanllenge, SIM_BETA);
  vector_print (*result, "przed:");

  for (int i = 0; i < TRAIN_REPEATS; i++) {
    index = get_random_int (0, OUTPUTS);
    t = matrix_to_vector (*identity_matrix, index);
    weights = train (*weights, examples[index], *t, 1, 0.1);
  }
  result = sim (*weights, letter_chanllenge, SIM_BETA);
  vector_print (*result, "po:");

  return EXIT_SUCCESS;
}
